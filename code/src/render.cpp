﻿#include <GL\glew.h>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <cstdio>
#include <cassert>
#include <iostream>

#include "GL_framework.h"
#include <vector>

#include <imgui\imgui.h>
#include <imgui\imgui_impl_sdl_gl3.h>

using namespace std;

//variables to load an object:

std::vector< glm::vec3 > verticesChicken;
std::vector< glm::vec2 > uvsChicken;
std::vector< glm::vec3 > normalsChicken;

std::vector< glm::vec3 > verticesTrump;
std::vector< glm::vec2 > uvsTrump;
std::vector< glm::vec3 > normalsTrump;

std::vector< glm::vec3 > verticesCabina;
std::vector< glm::vec2 > uvsCabina;
std::vector< glm::vec3 > normalsCabina;

std::vector< glm::vec3 > verticesPeus;
std::vector< glm::vec2 > uvsPeus;
std::vector< glm::vec3 > normalsPeus;

std::vector< glm::vec3 > verticesNoria;
std::vector< glm::vec2 > uvsNoria;
std::vector< glm::vec3 > normalsNoria;

std::vector< glm::vec3 > verticesCube;
std::vector< glm::vec2 > uvsCube;
std::vector< glm::vec3 > normalsCube;

glm::vec3 lightPos;
glm::vec3 lightPos2;
glm::vec3 lightPos3;

#define PI  3.14159265358979323846f 


extern bool loadOBJ(const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
);

extern bool loadOBJ1(const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
);

extern bool loadOBJ2(const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
);

extern bool loadOBJ3(const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
);

extern bool loadOBJ4(const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
);

extern bool loadOBJ5(const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
);

bool show_test_window = false;


bool light_moves = true;
bool on = false;
bool toon_1 = false;
bool toon_2 = false;
bool toon_3 = false;
void GUI() {
	bool show = true;
	ImGui::Begin("Simulation Parameters", &show, 0);

	// Do your GUI code here....
	{
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);//FrameRate


		if (ImGui::Button("Toggle Light Move")) {
			light_moves = !light_moves;

		}


	}
	// .........................

	ImGui::End();

	// Example code -- ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	if (show_test_window) {
		ImGui::SetNextWindowPos(ImVec2(650, 60), ImGuiSetCond_FirstUseEver);
		ImGui::ShowTestWindow(&show_test_window);
	}
}

///////// fw decl
namespace ImGui {
	void Render();
}
namespace Box {
void setupCube();
void cleanupCube();
void drawCube();
}
namespace Axis {
void setupAxis();
void cleanupAxis();
void drawAxis();
}

namespace MyLoadedModelChicken {
	void setupModel();
	void cleanupModel();
	void updateModel(const glm::mat4& transform);
	void drawModel(double currenTime);
	void drawModel2(double currentTime);
}

namespace MyLoadedModelTrump {
	void setupModel();
	void cleanupModel();
	void updateModel(const glm::mat4& transform);
	void drawModel(double currentTime);
	void drawModel2(double currentTime);
}

namespace MyLoadedModelCabina {
	void setupModel();
	void cleanupModel();
	void updateModel(const glm::mat4& transform);
	void drawModel(double currentTime);
	void drawModel2(double currentTime);
}

namespace MyLoadedModelPiesCabina {
	void setupModel();
	void cleanupModel();
	void updateModel(const glm::mat4& transform);
	void drawModel(double currentTime);
	void drawModel2(double currentTime);
	
}

namespace MyLoadedModelRodaCabina {
	void setupModel();
	void cleanupModel();
	void updateModel(const glm::mat4& transform);
	void drawModel(double currentTime);
	void drawModel2(double currentTime);
}

namespace MyLoadedModelCube {
	void setupModel();
	void cleanupModel();
	void updateModel(const glm::mat4& transform);
	void drawModel(double currentTime);
}

namespace Sphere {
	void setupSphere(glm::vec3 pos, float radius);
	void cleanupSphere();
	void updateSphere(glm::vec3 pos, float radius);
	void drawSphere();
}

namespace Sphere2 {
	void setupSphere(glm::vec3 pos, float radius);
	void cleanupSphere();
	void updateSphere(glm::vec3 pos, float radius);
	void drawSphere();
}

namespace Sphere3 {
	void setupSphere(glm::vec3 pos, float radius);
	void cleanupSphere();
	void updateSphere(glm::vec3 pos, float radius);
	void updateModel(const glm::mat4& transform);
	void drawSphere(double currentTime);
}



////////////////

namespace RenderVars {
	const float FOV = glm::radians(65.f);
	const float zNear = 1.f;
	const float zFar = 5000.f;

	glm::mat4 _projection;
	glm::mat4 _modelView;
	glm::mat4 _MVP;
	glm::mat4 _inv_modelview;
	glm::vec4 _cameraPoint;

	struct prevMouse {
		float lastx, lasty;
		MouseEvent::Button button = MouseEvent::Button::None;
		bool waspressed = false;
	} prevMouse;

	float panv[3] = { 0.f, 0.f, -75.f };
	float rota[2] = { 0.f, 0.f };
}
namespace RV = RenderVars;

void GLResize(int width, int height) {
	glViewport(0, 0, width, height);
	if(height != 0) RV::_projection = glm::perspective(RV::FOV, (float)width / (float)height, RV::zNear, RV::zFar);
	else RV::_projection = glm::perspective(RV::FOV, 0.f, RV::zNear, RV::zFar);
}

void GLmousecb(MouseEvent ev) {
	if(RV::prevMouse.waspressed && RV::prevMouse.button == ev.button) {
		float diffx = ev.posx - RV::prevMouse.lastx;
		float diffy = ev.posy - RV::prevMouse.lasty;
		switch(ev.button) {
		case MouseEvent::Button::Left: // ROTATE
			RV::rota[0] += diffx * 0.005f;
			RV::rota[1] += diffy * 0.005f;
			break;
		case MouseEvent::Button::Right: // MOVE XY
			RV::panv[0] += diffx * 0.03f;
			RV::panv[1] -= diffy * 0.03f;
			break;
		case MouseEvent::Button::Middle: // MOVE Z
			RV::panv[2] += diffy * 0.05f;
			break;
		default: break;
		}
	} else {
		RV::prevMouse.button = ev.button;
		RV::prevMouse.waspressed = true;
	}
	RV::prevMouse.lastx = ev.posx;
	RV::prevMouse.lasty = ev.posy;
}

void GLinit(int width, int height) {
	glViewport(0, 0, width, height);
	glClearColor(0.2f, 0.2f, 0.2f, 1.f);
	glClearDepth(1.f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	RV::_projection = glm::perspective(RV::FOV, (float)width/(float)height, RV::zNear, RV::zFar);

	// Setup shaders & geometry
	/*Box::setupCube();
	Axis::setupAxis();*/

	//bool res = loadOBJ("farola.obj", vertices, uvs, normals);

	bool res = loadOBJ("Chicken.obj", verticesChicken, uvsChicken, normalsChicken);

	bool res1 = loadOBJ1("Trump.obj", verticesTrump, uvsTrump, normalsTrump);

	bool res2 = loadOBJ2("test_cabin_v3.obj", verticesCabina, uvsCabina, normalsCabina);

	bool res3 = loadOBJ3("peus.obj", verticesPeus, uvsPeus, normalsPeus);

	bool res4 = loadOBJ4("noria.obj", verticesNoria, uvsNoria, normalsNoria);

	bool res5 = loadOBJ5("cube.obj", verticesCube, uvsCube, normalsCube);
		
	
	MyLoadedModelChicken::setupModel();
	MyLoadedModelTrump::setupModel();
	MyLoadedModelCabina::setupModel();
	MyLoadedModelPiesCabina::setupModel();
	MyLoadedModelRodaCabina::setupModel();
	MyLoadedModelCube::setupModel();

	lightPos =  glm::vec3(40, 40, 0);
	lightPos2 = glm::vec3(40, 40, 0);
	lightPos3 = glm::vec3(0, 0, 0);

	Sphere::setupSphere(lightPos, 1.0f);
	Sphere2::setupSphere(lightPos2, 1.0f);
	Sphere3::setupSphere(lightPos3, 1.0f);



}

void GLcleanup() {
	/*Box::cleanupCube();
	Axis::cleanupAxis();*/

	MyLoadedModelChicken::cleanupModel();
	MyLoadedModelTrump::cleanupModel();
	MyLoadedModelCabina::cleanupModel();
	MyLoadedModelPiesCabina::cleanupModel();
	MyLoadedModelRodaCabina::cleanupModel();
	MyLoadedModelCube::cleanupModel();

	Sphere::cleanupSphere();
	Sphere2::cleanupSphere();
	Sphere3::cleanupSphere();


}

void GLrender(double currentTime, int tecla , int teclaC, bool teclaD, bool teclaB, bool teclaT) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	RV::_modelView = glm::mat4(1.f);
	RV::_modelView = glm::translate(RV::_modelView, glm::vec3(RV::panv[0], RV::panv[1], RV::panv[2]));
	RV::_modelView = glm::rotate(RV::_modelView, RV::rota[1], glm::vec3(1.f, 0.f, 0.f));
	RV::_modelView = glm::rotate(RV::_modelView, RV::rota[0], glm::vec3(0.f, 1.f, 0.f));

	// render code
	/*Box::drawCube();
	Axis::drawAxis();*/

	int max_cabins = 20;
	float frequencia = 0.05f;
	float radius = 22.0f;

	if (light_moves) {
		lightPos = glm::vec3(67.5f*sin((float)currentTime/3), -100 * sin((float)currentTime/3), -100 * cos((float)currentTime/3));
		lightPos2 = glm::vec3(67.5f*sin((float)currentTime/3), 100 * sin((float)currentTime/3), 100 * cos((float)currentTime/3));
		//lightPos3 = glm::vec3(0.0f, 0.0f, 0.0f);//glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0);
	}
	//rad:(135*180/PI)
   // lightPos3 = glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0);
	
	Sphere::updateSphere(lightPos, 50.0f);
	Sphere2::updateSphere(lightPos2, 50.0f);
	Sphere3::updateSphere(lightPos3, 1.0f);
	Sphere::drawSphere();
	Sphere2::drawSphere();
	
	if (tecla == 0) { //Exercici 1 Cubs
		MyLoadedModelCube::drawModel(currentTime);
		
		//MyLoadedModelPiesCabina::drawModel();
	}
	if (tecla == 1) { //Exercici 2 Noria
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
						
	}
	if (tecla == 2) { //Exercici 3 Plans
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
		
		glm::mat4 t;
		glm::mat4 r;
		glm::mat4 r2;
		int time = currentTime;
		float valorActualTime = time % 2;

		if (valorActualTime == 0) {
			//GALLINA
			t = glm::translate(glm::mat4(), glm::vec3(1.f, 1.0f, 0.5f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
			r = glm::rotate(glm::mat4(), (3 * PI) / 2, glm::vec3(0.0f, -1.0, 0.0f));
			r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(1.0f, 0.f, 0.f));

			RV::_modelView = r2 * r * t;
		}
		else
		{
			//TRUMP
			t = glm::translate(glm::mat4(), glm::vec3(-1.0f, 2.5f, 0.5f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
			r = glm::rotate(glm::mat4(), PI / 2.f, glm::vec3(0.0f, -1.f, 0.f));
			r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(-1.0f, 0.f, 0.f));

			RV::_modelView = r2 * r * t;
		}
				
	}
	if (tecla == 3) { //Exercici 4 Camara 

		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);

		glm::mat4 t;
		glm::mat4 r;
		glm::mat4 r2;
		int time = currentTime;
		float valorActualTime = time % 2;

		if (teclaC == 0) {

		}
		else if (teclaC == 1) {
			if (valorActualTime == 0) {
				//GALLINA
				t = glm::translate(glm::mat4(), glm::vec3(1.f, 1.0f, 0.5f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), (3 * PI) / 2, glm::vec3(0.0f, -1.0, 0.0f));
				r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(1.0f, 0.f, 0.f));
								
				RV::_modelView = r2 * r * t;
			}
			else
			{
				//TRUMP
				t = glm::translate(glm::mat4(), glm::vec3(-1.0f, 2.5f, 0.5f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), PI / 2.f, glm::vec3(0.0f, -1.f, 0.f));
				r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(-1.0f, 0.f, 0.f));
				
				RV::_modelView = r2 * r * t;
			}

		}
		else if (teclaC == 2) {
			t = glm::translate(glm::mat4(), glm::vec3(0.0f, 2.5f, -50.0f));
			r = glm::rotate(glm::mat4(), PI/3 , glm::vec3(0.0f, -1.0f, 0.f));
			//r2 = glm::rotate(glm::mat4(), PI / 6, glm::vec3(0.0f, 1.0f, 0.f));
			RV::_modelView = t*r;
		}
		else if (teclaC == 3) {
			t = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, 0.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
			r = glm::rotate(glm::mat4(), PI / 2, glm::vec3(1.0f, 0.0f, 0.f));
			float timeRadiants = glm::radians(currentTime * PI * 3.0f);
			r2 = glm::rotate(glm::mat4(), timeRadiants, glm::vec3(0.0f, 1.0f, 0.0f));

			 glm::mat4 rt = r * r2;
			 RV::_modelView = rt * t;
		}

	}
	if (tecla == 4) { // Exercici 5 Dia Nit
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
		glm::mat4 t;
		glm::mat4 r;
		glm::mat4 r2;
		int time = currentTime;
		float valorActualTime = time % 2;

		if (teclaC == 0) {

		}
		else if (teclaC == 1) {
			if (valorActualTime == 0) {
				//GALLINA
				t = glm::translate(glm::mat4(), glm::vec3(1.f, 1.0f, 0.5f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), (3 * PI) / 2, glm::vec3(0.0f, -1.0, 0.0f));
				r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(1.0f, 0.f, 0.f));

				RV::_modelView = r2 * r * t;
			}
			else
			{
				//TRUMP
				t = glm::translate(glm::mat4(), glm::vec3(-1.0f, 2.5f, 0.5f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), PI / 2.f, glm::vec3(0.0f, -1.f, 0.f));
				r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(-1.0f, 0.f, 0.f));

				RV::_modelView = r2 * r * t;
			}

		}
		else if (teclaC == 2) {
			t = glm::translate(glm::mat4(), glm::vec3(0.0f, 2.5f, -50.0f));
			r = glm::rotate(glm::mat4(), PI / 3, glm::vec3(0.0f, -1.0f, 0.f));
			//r2 = glm::rotate(glm::mat4(), PI / 6, glm::vec3(0.0f, 1.0f, 0.f));
			RV::_modelView = t*r;
		}
		else if (teclaC == 3) {
			t = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, 0.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
			r = glm::rotate(glm::mat4(), PI / 2, glm::vec3(1.0f, 0.0f, 0.f));
			float timeRadiants = glm::radians(currentTime * PI * 3.0f);
			r2 = glm::rotate(glm::mat4(), timeRadiants, glm::vec3(0.0f, 1.0f, 0.0f));

			glm::mat4 rt = r * r2;
			RV::_modelView = rt * t;
		}
	}
	if (tecla == 5) { //exercici 6 Bombeta
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
		Sphere3::drawSphere(currentTime);
		glm::mat4 t;
		int time = currentTime;
		float valorActualTime = time % 2;

		t = glm::translate(glm::mat4(),glm::vec3(0.0f,1.0f,-4.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
		
		RV::_modelView = t;
		lightPos3 = glm::vec3(0.0f, 0.0f, 0.0f);
		if (teclaD) {
			light_moves = false;
		}

		else {
			light_moves = true;
		}

		if (teclaB) {
			on = false;
		}
		else {
			on = true;
		}
	}
	if (tecla == 6) { //exercici 7 Pendol
		on = true;
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
		Sphere3::drawSphere(currentTime);
		glm::mat4 t;
		int time = currentTime;
		float valorActualTime = time % 2;

		t = glm::translate(glm::mat4(), glm::vec3(0.0f, 1.0f, -4.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));

		RV::_modelView = t;
		lightPos3 = glm::vec3(3* cos(currentTime), 0.0f , 0.0f);
		if (teclaD) {
			light_moves = false;
		}

		else {
			light_moves = true;
		}

		if (teclaB) {
			on = false;
		}
		else {
			on = true;
		}
	}
	if (tecla == 7) { // Exercici 8 2 nories
		light_moves = true;
		MyLoadedModelChicken::drawModel2(currentTime);
		MyLoadedModelTrump::drawModel2(currentTime);
		MyLoadedModelCabina::drawModel2(currentTime);
		MyLoadedModelPiesCabina::drawModel2(currentTime);
		MyLoadedModelRodaCabina::drawModel2(currentTime);

		glm::mat4 t;
		glm::mat4 r;
		glm::mat4 r2;
		int time = currentTime;
		float valorActualTime = time % 2;

		if (teclaC == 0) {

			t = glm::translate(glm::mat4(), glm::vec3(-30.f, 0.0f, -80.0f));
			RV::_modelView = r * t;

		}
		else if (teclaC == 1) {

			//float a = radius * cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins);
			float b = radius * sin(2 * PI *frequencia*currentTime + 2.0f * PI * 11.0f / max_cabins);
			//cout << "printo el valor de la suma" << b << endl;

			if (b <= 0) {

				//TRUMP
				t = glm::translate(glm::mat4(), glm::vec3(-1.0f, 2.5f, 0.5f) + glm::vec3(radius* sin(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*cos(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), PI / 2.f, glm::vec3(0.0f, -1.f, 0.f));
				r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(-1.0f, 0.f, 0.f));


				if (b == 22) {
					t = glm::translate(glm::mat4(), glm::vec3(-60.0f, 0.f, 0.0f));
					RV::_modelView = t;
				}
				RV::_modelView = r2 * r * t;


			}
			else
			{

				//GALLINA
				t = glm::translate(glm::mat4(), glm::vec3(-59.f, 1.0f, 0.5f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), (3 * PI) / 2, glm::vec3(0.0f, -1.0, 0.0f));
				r2 = glm::rotate(glm::mat4(), PI / 6.f, glm::vec3(1.0f, 0.f, 0.f));

				if (b == 22) {
					t = glm::translate(glm::mat4(), glm::vec3(60.0f, 0.f, 0.0f));
					RV::_modelView = t;
				}
				RV::_modelView = r2 * r * t;


			}

		}
		else if (teclaC == 2) {
			t = glm::translate(glm::mat4(), glm::vec3(-20.0f, -5.f, -125.0f));
			r = glm::rotate(glm::mat4(), PI / 3, glm::vec3(0.0f, -1.0f, 0.f));
			//r2 = glm::rotate(glm::mat4(), PI / 6, glm::vec3(0.0f, 1.0f, 0.f));
			RV::_modelView = t * r;
		}
		else if (teclaC == 3) {
			float b = radius * sin(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins);
			if (b <= 0) {

				t = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, 0.0f) + glm::vec3(radius* sin(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*cos(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), PI / 2, glm::vec3(1.0f, 0.0f, 0.f));
				float timeRadiants = glm::radians(currentTime * PI * 3);
				r2 = glm::rotate(glm::mat4(), timeRadiants, glm::vec3(0.0f, 1.0f, 0.0f));
				glm::mat4 rt = r * r2;
				RV::_modelView = rt * t;


			} //NOMES FUNCIONA LA PRIMERA MEITAT DE LA ROTACIO
			else {

				t = glm::translate(glm::mat4(), glm::vec3(0.f, 0.f, 0.0f) + glm::vec3(radius* sin(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*cos(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));
				r = glm::rotate(glm::mat4(), PI / 2, glm::vec3(-1.0f, 0.0f, 0.f));
				float timeRadiants = glm::radians(currentTime * PI * 3);
				r2 = glm::rotate(glm::mat4(), timeRadiants, glm::vec3(0.0f, 1.0f, 0.0f));
				glm::mat4 rt = r2 * r;
				RV::_modelView = rt * t;

			}


		}

	}
	if (tecla == 8) { // Exercici 9 Toon Shader 1
		toon_2 = false;
		toon_3 = false;
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
		Sphere3::drawSphere(currentTime);
		glm::mat4 t;
		int time = currentTime;
		float valorActualTime = time % 2;

		t = glm::translate(glm::mat4(), glm::vec3(0.0f, 1.0f, -4.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));

		RV::_modelView = t;
		lightPos3 = glm::vec3(0.0f, 0.0f, 0.0f);
		if (teclaD) {
			light_moves = false;
		}

		else {
			light_moves = true;
		}

		if (teclaB) {
			on = false;
		}
		else {
			on = true;
		}

		if (teclaT) {
			toon_1 = false;
		}
		else {
			toon_1 = true;
		}

	}
	if (tecla == 9) { // Exercici 10 Toon Shader 2
		toon_1 = false;
		toon_3 = false;
		light_moves = true;
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
		Sphere3::drawSphere(currentTime);
		glm::mat4 t;
		int time = currentTime;
		float valorActualTime = time % 2;

		t = glm::translate(glm::mat4(), glm::vec3(0.0f, 1.0f, -4.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));

		RV::_modelView = t;
		lightPos3 = glm::vec3(0.0f, 0.0f, 0.0f);
		if (teclaD) {
			light_moves = false;
		}

		else {
			light_moves = true;
		}

		if (teclaB) {
			on = false;
		}
		else {
			on = true;
		}

		if (teclaT) {
			toon_2 = false;
		}
		else {
			toon_2 = true;
		}
	}
	if (tecla == 10) { // Exercici 11 Toon Shader 3
		toon_1 = false;
		toon_2 = false;
		light_moves = false;
		MyLoadedModelChicken::drawModel(currentTime);
		MyLoadedModelTrump::drawModel(currentTime);
		MyLoadedModelCabina::drawModel(currentTime);
		MyLoadedModelPiesCabina::drawModel(currentTime);
		MyLoadedModelRodaCabina::drawModel(currentTime);
		Sphere3::drawSphere(currentTime);
		glm::mat4 t;
		int time = currentTime;
		float valorActualTime = time % 2;

		t = glm::translate(glm::mat4(), glm::vec3(0.0f, 1.0f, -4.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 11 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 11 / max_cabins), 0));

		RV::_modelView = t;
		lightPos3 = glm::vec3(0.0f, 0.0f, 0.0f);
		if (teclaD) {
			light_moves = false;
		}

		else {
			light_moves = true;
		}

		if (teclaB) {
			on = false;
		}
		else {
			on = true;
		}

		if (teclaT) {
			toon_3 = false;
		}
		else {
			toon_3 = true;
		}

	}

	RV::_MVP = RV::_projection * RV::_modelView;
	
	ImGui::Render();
}


//////////////////////////////////// COMPILE AND LINK
GLuint compileShader(const char* shaderStr, GLenum shaderType, const char* name="") {
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderStr, NULL);
	glCompileShader(shader);
	GLint res;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &res);
	if (res == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetShaderInfoLog(shader, res, &res, buff);
		fprintf(stderr, "Error Shader %s: %s", name, buff);
		delete[] buff;
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}
void linkProgram(GLuint program) {
	glLinkProgram(program);
	GLint res;
	glGetProgramiv(program, GL_LINK_STATUS, &res);
	if (res == GL_FALSE) {
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetProgramInfoLog(program, res, &res, buff);
		fprintf(stderr, "Error Link: %s", buff);
		delete[] buff;
	}
}

////////////////////////////////////////////////// MyModelChicken
namespace MyLoadedModelChicken {
	GLuint modelVao;
	GLuint modelVbo[3];
	GLuint modelShaders[2];
	GLuint modelProgram;
	glm::mat4 objMat = glm::mat4(1.f);



	const char* model_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	uniform vec3 lPos;\n\
	uniform vec3 lPos2;\n\
	uniform vec3 lPos3;\n\
	out vec3 lDir;\n\
	out vec3 lDir2;\n\
	out vec3 lDir3;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
		lDir = normalize(lPos - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir2 = normalize(lPos2 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir3 = normalize(lPos3 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
	}";


	const char* model_fragShader =
		"#version 330\n\
	in vec4 vert_Normal;\n\
	in vec3 lDir;\n\
	in vec3 lDir2;\n\
	in vec3 lDir3;\n\
	out vec4 out_Color;\n\
	uniform mat4 mv_Mat;\n\
	uniform vec4 color;\n\
	uniform vec4 colorlight2;\n\
	uniform vec4 colorlight3;\n\
	uniform vec4 colorambient;\n\
	uniform bool move;\n\
	uniform bool on;\n\
	uniform bool toon_1;\n\
	uniform bool toon_2;\n\
	uniform bool toon_3;\n\
	void main() {\n\
	  float difuse = dot(normalize(vert_Normal), mv_Mat*vec4(lDir.x, lDir.y, lDir.z, 0.0)); \n\
 	  float difuse2 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir2.x, lDir2.y, lDir2.z, 0.0));\n\
	  float difuse3 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir3.x, lDir3.y, lDir3.z, 0.0));\n\
		if(move){\n\
			if(lDir.y > 0){\n\
			out_Color = vec4(min(color.xyz * difuse + colorambient.xyz * 0.3, 1), 1.0 );\n\
			}\n\
			else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0 );\n\
			}\n\
		}else{\n\
			if(on){\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3 + colorambient.xyz *0.3, 1.0), 1.0);\n\
			}else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0); \n\
			}\n\
		}\n\
		if(toon_1){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			out_Color = vec4(min(color.xyz * difuse, 1), 1.0 );\n\
		}\n\
		if(toon_2){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			\n\
			if(difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			out_Color = vec4(min(color.xyz * difuse + colorlight2.xyz * difuse2, 1), 1.0); \n\
		}\n\
		if(toon_3){\n\
			if (difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			if (difuse3 < 0.2) difuse3 = 0; \n\
			if (difuse3 <= 0.2 && difuse3 < 0.4) difuse3 = 0.2; \n\
			if (difuse3 >= 0.4 && difuse3 < 0.5) difuse3 = 0.4; \n\
			if (difuse3 >= 0.5) difuse3 = 1; \n\
			\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3, 1), 1.0); \n\
			}\n\
		}";
	/*
	Toon Shader
		if(difuse < 0.2) difuse = 0; \n\
		if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
		if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
		if (difuse >= 0.5) difuse = 1; \n\
	*/
	void setupModel() {
		glGenVertexArrays(1, &modelVao);
		glBindVertexArray(modelVao);
		glGenBuffers(3, modelVbo);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[0]);

		glBufferData(GL_ARRAY_BUFFER, verticesChicken.size() * sizeof(glm::vec3), &verticesChicken[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[1]);

		glBufferData(GL_ARRAY_BUFFER, normalsChicken.size() * sizeof(glm::vec3), &normalsChicken[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);



		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		modelShaders[0] = compileShader(model_vertShader, GL_VERTEX_SHADER, "cubeVert");
		modelShaders[1] = compileShader(model_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		modelProgram = glCreateProgram();
		glAttachShader(modelProgram, modelShaders[0]);
		glAttachShader(modelProgram, modelShaders[1]);
		glBindAttribLocation(modelProgram, 0, "in_Position");
		glBindAttribLocation(modelProgram, 1, "in_Normal");
		linkProgram(modelProgram);
	}
	void cleanupModel() {

		glDeleteBuffers(2, modelVbo);
		glDeleteVertexArrays(1, &modelVao);

		glDeleteProgram(modelProgram);
		glDeleteShader(modelShaders[0]);
		glDeleteShader(modelShaders[1]);
	}
	void updateModel(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawModel(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.02f;  //CHIKEN
		
		glm::mat4 s;
		glm::mat4 t;
		glm::mat4 r;
		s = glm::scale(glm::mat4(), glm::vec3(scal));
		int max_cabins = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;
		t = glm::translate(glm::mat4(), glm::vec3(1.0f, -3.0f, 0.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0));
		r = glm::rotate(glm::mat4(), (3*PI)/2, glm::vec3(0.0f,1.0f, 0.0f));

		objMat = t*s*r;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glDrawArrays(GL_TRIANGLES, 0, 100000);

		

		glUseProgram(0);
		glBindVertexArray(0);

	}
	void drawModel2(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.02f;  //CHIKEN

		glm::mat4 s;
		glm::mat4 t;
		glm::mat4 r;
		s = glm::scale(glm::mat4(), glm::vec3(scal));
		int max_cabins = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;
		t = glm::translate(glm::mat4(), glm::vec3(1.0f, -3.0f, 0.0f) + glm::vec3(radius* sin(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*cos(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0));
		r = glm::rotate(glm::mat4(), (3 * PI) / 2, glm::vec3(0.0f, 1.0, 0.0f));

		objMat = t * s*r;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glDrawArrays(GL_TRIANGLES, 0, 100000);

		s = glm::scale(glm::mat4(), glm::vec3(scal));
		t = glm::translate(glm::mat4(), glm::vec3(61.0f, -3.0f, 0.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0));
		r = glm::rotate(glm::mat4(), (3 * PI) / 2, glm::vec3(0.0f, 1.0, 0.0f));

		//t = glm::translate(glm::mat4(), glm::vec3(60.0f, 0.0f, 0.0f));
		objMat = t * s*r;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glDrawArrays(GL_TRIANGLES, 0, 100000);


		glUseProgram(0);
		glBindVertexArray(0);

	}


}

////////////////////////////////////////////////// MyModelTrump
namespace MyLoadedModelTrump {
	GLuint modelVao;
	GLuint modelVbo[3];
	GLuint modelShaders[2];
	GLuint modelProgram;
	glm::mat4 objMat = glm::mat4(1.f);




	const char* model_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	uniform vec3 lPos;\n\
	uniform vec3 lPos2;\n\
	uniform vec3 lPos3;\n\
	out vec3 lDir;\n\
	out vec3 lDir2;\n\
	out vec3 lDir3;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
		lDir = normalize(lPos - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir2 = normalize(lPos2 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir3 = normalize(lPos3 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
	}";


	const char* model_fragShader =
		"#version 330\n\
	in vec4 vert_Normal;\n\
	in vec3 lDir;\n\
	in vec3 lDir2;\n\
	in vec3 lDir3;\n\
	out vec4 out_Color;\n\
	uniform mat4 mv_Mat;\n\
	uniform vec4 color;\n\
	uniform vec4 colorlight2;\n\
	uniform vec4 colorlight3;\n\
	uniform vec4 colorambient;\n\
	uniform bool move;\n\
	uniform bool on;\n\
	uniform bool toon_1;\n\
	uniform bool toon_2;\n\
	uniform bool toon_3;\n\
	void main() {\n\
	  float difuse = dot(normalize(vert_Normal), mv_Mat*vec4(lDir.x, lDir.y, lDir.z, 0.0)); \n\
 	  float difuse2 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir2.x, lDir2.y, lDir2.z, 0.0));\n\
	  float difuse3 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir3.x, lDir3.y, lDir3.z, 0.0));\n\
		if(move){\n\
			if(lDir.y > 0){\n\
			out_Color = vec4(min(color.xyz * difuse + colorambient.xyz * 0.3, 1), 1.0 );\n\
			}\n\
			else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0 );\n\
			}\n\
		}else{\n\
			if(on){\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3 + colorambient.xyz *0.3, 1.0), 1.0);\n\
			}else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0); \n\
			}\n\
		}\n\
		if(toon_1){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			out_Color = vec4(min(color.xyz * difuse, 1), 1.0 );\n\
		}\n\
		if(toon_2){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			\n\
			if(difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			out_Color = vec4(min(color.xyz * difuse + colorlight2.xyz * difuse2, 1), 1.0); \n\
		}\n\
		if(toon_3){\n\
			if (difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			if (difuse3 < 0.2) difuse3 = 0; \n\
			if (difuse3 <= 0.2 && difuse3 < 0.4) difuse3 = 0.2; \n\
			if (difuse3 >= 0.4 && difuse3 < 0.5) difuse3 = 0.4; \n\
			if (difuse3 >= 0.5) difuse3 = 1; \n\
			\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3, 1), 1.0); \n\
			}\n\
		}";
	void setupModel() {
		glGenVertexArrays(1, &modelVao);
		glBindVertexArray(modelVao);
		glGenBuffers(3, modelVbo);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[0]);

		glBufferData(GL_ARRAY_BUFFER, verticesTrump.size() * sizeof(glm::vec3), &verticesTrump[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[1]);

		glBufferData(GL_ARRAY_BUFFER, normalsTrump.size() * sizeof(glm::vec3), &normalsTrump[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);



		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		modelShaders[0] = compileShader(model_vertShader, GL_VERTEX_SHADER, "cubeVert");
		modelShaders[1] = compileShader(model_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		modelProgram = glCreateProgram();
		glAttachShader(modelProgram, modelShaders[0]);
		glAttachShader(modelProgram, modelShaders[1]);
		glBindAttribLocation(modelProgram, 0, "in_Position");
		glBindAttribLocation(modelProgram, 1, "in_Normal");
		linkProgram(modelProgram);
	}
	void cleanupModel() {

		glDeleteBuffers(2, modelVbo);
		glDeleteVertexArrays(1, &modelVao);

		glDeleteProgram(modelProgram);
		glDeleteShader(modelShaders[0]);
		glDeleteShader(modelShaders[1]);
	}
	void updateModel(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawModel(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		
		float scal = 0.01f; //TRUMP
							
		glm::mat4 s;
		glm::mat4 t;
		glm::mat4 r;
		s = glm::scale(glm::mat4(), glm::vec3(scal));
		int max_cabins = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;
		t = glm::translate(glm::mat4(), glm::vec3(-1.0f,-3.0f,0.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0));
		r = glm::rotate(glm::mat4(), PI/2, glm::vec3(0.0f, 1.0, 0.0f));
		objMat = t*s*r;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glDrawArrays(GL_TRIANGLES, 0, 100000);


		glUseProgram(0);
		glBindVertexArray(0);

	}

	void drawModel2(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);


		float scal = 0.01f; //TRUMP

		glm::mat4 s;
		glm::mat4 t;
		glm::mat4 r;
		s = glm::scale(glm::mat4(), glm::vec3(scal));
		int max_cabins = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;
		t = glm::translate(glm::mat4(), glm::vec3(-1.0f, -3.0f, 0.0f) + glm::vec3(radius* sin(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*cos(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0));
		r = glm::rotate(glm::mat4(), PI / 2, glm::vec3(0.0f, 1.0, 0.0f));
		objMat = t * s *r;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glDrawArrays(GL_TRIANGLES, 0, 100000);

		s = glm::scale(glm::mat4(), glm::vec3(scal));
		t = glm::translate(glm::mat4(), glm::vec3(59.0f, -3.0f, 0.0f) + glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 15 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 15 / max_cabins), 0));
		r = glm::rotate(glm::mat4(), PI / 2, glm::vec3(1.0f, 0.0, 0.0f));

		objMat = t * s *r;


		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glDrawArrays(GL_TRIANGLES, 0, 100000);

		glUseProgram(0);
		glBindVertexArray(0);

	}


}

////////////////////////////////////////////////// MyModelCabina
namespace MyLoadedModelCabina {
	GLuint modelVao;
	GLuint modelVbo[3];
	GLuint modelShaders[2];
	GLuint modelProgram;
	glm::mat4 objMat = glm::mat4(1.f);




	const char* model_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	uniform vec3 lPos;\n\
	uniform vec3 lPos2;\n\
	uniform vec3 lPos3;\n\
	out vec3 lDir;\n\
	out vec3 lDir2;\n\
	out vec3 lDir3;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
		lDir = normalize(lPos - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir2 = normalize(lPos2 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir3 = normalize(lPos3 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
	}";


	const char* model_fragShader =
		"#version 330\n\
	in vec4 vert_Normal;\n\
	in vec3 lDir;\n\
	in vec3 lDir2;\n\
	in vec3 lDir3;\n\
	out vec4 out_Color;\n\
	uniform mat4 mv_Mat;\n\
	uniform vec4 color;\n\
	uniform vec4 colorlight2;\n\
	uniform vec4 colorlight3;\n\
	uniform vec4 colorambient;\n\
	uniform bool move;\n\
	uniform bool on;\n\
	uniform bool toon_1;\n\
	uniform bool toon_2;\n\
	uniform bool toon_3;\n\
	void main() {\n\
	  float difuse = dot(normalize(vert_Normal), mv_Mat*vec4(lDir.x, lDir.y, lDir.z, 0.0)); \n\
 	  float difuse2 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir2.x, lDir2.y, lDir2.z, 0.0));\n\
	  float difuse3 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir3.x, lDir3.y, lDir3.z, 0.0));\n\
		if(move){\n\
			if(lDir.y > 0){\n\
			out_Color = vec4(min(color.xyz * difuse + colorambient.xyz * 0.3, 1), 1.0 );\n\
			}\n\
			else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0 );\n\
			}\n\
		}else{\n\
			if(on){\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3 + colorambient.xyz *0.3, 1.0), 1.0);\n\
			}else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0); \n\
			}\n\
		}\n\
		if(toon_1){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			out_Color = vec4(min(color.xyz * difuse, 1), 1.0 );\n\
		}\n\
		if(toon_2){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			\n\
			if(difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			out_Color = vec4(min(color.xyz * difuse + colorlight2.xyz * difuse2, 1), 1.0); \n\
		}\n\
		if(toon_3){\n\
			if (difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			if (difuse3 < 0.2) difuse3 = 0; \n\
			if (difuse3 <= 0.2 && difuse3 < 0.4) difuse3 = 0.2; \n\
			if (difuse3 >= 0.4 && difuse3 < 0.5) difuse3 = 0.4; \n\
			if (difuse3 >= 0.5) difuse3 = 1; \n\
			\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3, 1), 1.0); \n\
			}\n\
		}";

	void setupModel() {
		glGenVertexArrays(1, &modelVao);
		glBindVertexArray(modelVao);
		glGenBuffers(3, modelVbo);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[0]);

		glBufferData(GL_ARRAY_BUFFER, verticesCabina.size() * sizeof(glm::vec3), &verticesCabina[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[1]);

		glBufferData(GL_ARRAY_BUFFER, normalsCabina.size() * sizeof(glm::vec3), &normalsCabina[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);



		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		modelShaders[0] = compileShader(model_vertShader, GL_VERTEX_SHADER, "cubeVert");
		modelShaders[1] = compileShader(model_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		modelProgram = glCreateProgram();
		glAttachShader(modelProgram, modelShaders[0]);
		glAttachShader(modelProgram, modelShaders[1]);
		glBindAttribLocation(modelProgram, 0, "in_Position");
		glBindAttribLocation(modelProgram, 1, "in_Normal");
		linkProgram(modelProgram);
	}
	void cleanupModel() {

		glDeleteBuffers(2, modelVbo);
		glDeleteVertexArrays(1, &modelVao);

		glDeleteProgram(modelProgram);
		glDeleteShader(modelShaders[0]);
		glDeleteShader(modelShaders[1]);
	}
	void updateModel(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawModel(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.005f; //NORIA CABINA

		glm::mat4 s;
		glm::mat4 t;

		//traslate * rotate * escalat 
		s = glm::scale(glm::mat4(), glm::vec3(scal));
		int max_cabins = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;
		
		for (int i = 0; i <= max_cabins; ++i) {
			t = glm::translate(glm::mat4(), glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * i / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * i / max_cabins), 0));

			objMat = t * s;

			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
			glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
			glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
			glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
			glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
			glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
			glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
			glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
			glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
			glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
			glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
			glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
			glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
			glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
			glDrawArrays(GL_TRIANGLES, 0, 100000);
			
			
		}


		glUseProgram(0);
		glBindVertexArray(0);

	}

	void drawModel2(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.005f; //NORIA CABINA

		glm::mat4 s;
		glm::mat4 t;
		glm::mat4 t2;

		//traslate * rotate * escalat 
		s = glm::scale(glm::mat4(), glm::vec3(scal));
		int max_cabins = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;

		for (int i = 0; i <= max_cabins; ++i) {
			t = glm::translate(glm::mat4(), glm::vec3(radius* cos(2 * PI *-frequencia * currentTime + 2 * PI * i / max_cabins), radius*sin(2 * PI*-frequencia * currentTime + 2 * PI * i / max_cabins), 0));

			objMat = t * s;

			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
			glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
			glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
			glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
			glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
			glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
			glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
			glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
			glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
			glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
			glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
			glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
			glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
			glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
			glDrawArrays(GL_TRIANGLES, 0, 100000);

			//s = glm::scale(glm::mat4(), glm::vec3(scal));
			t = glm::translate(glm::mat4(), glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * i / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * i / max_cabins), 0));
			t2 = glm::translate(glm::mat4(), glm::vec3(60.0f, 0.0f, 0.0f));
			objMat = t2 * t * s;

			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
			glDrawArrays(GL_TRIANGLES, 0, 100000);
		}



		glUseProgram(0);
		glBindVertexArray(0);

	}



}
////////////////////////////////////////////////// MyModelPeus
namespace MyLoadedModelPiesCabina {
	GLuint modelVao;
	GLuint modelVbo[3];
	GLuint modelShaders[2];
	GLuint modelProgram;
	glm::mat4 objMat = glm::mat4(1.f);



	const char* model_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	uniform vec3 lPos;\n\
	uniform vec3 lPos2;\n\
	uniform vec3 lPos3;\n\
	out vec3 lDir;\n\
	out vec3 lDir2;\n\
	out vec3 lDir3;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
		lDir = normalize(lPos - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir2 = normalize(lPos2 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir3 = normalize(lPos3 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
	}";


	const char* model_fragShader =
		"#version 330\n\
	in vec4 vert_Normal;\n\
	in vec3 lDir;\n\
	in vec3 lDir2;\n\
	in vec3 lDir3;\n\
	out vec4 out_Color;\n\
	uniform mat4 mv_Mat;\n\
	uniform vec4 color;\n\
	uniform vec4 colorlight2;\n\
	uniform vec4 colorlight3;\n\
	uniform vec4 colorambient;\n\
	uniform bool move;\n\
	uniform bool on;\n\
	uniform bool toon_1;\n\
	uniform bool toon_2;\n\
	uniform bool toon_3;\n\
	void main() {\n\
	  float difuse = dot(normalize(vert_Normal), mv_Mat*vec4(lDir.x, lDir.y, lDir.z, 0.0)); \n\
 	  float difuse2 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir2.x, lDir2.y, lDir2.z, 0.0));\n\
	  float difuse3 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir3.x, lDir3.y, lDir3.z, 0.0));\n\
		if(move){\n\
			if(lDir.y > 0){\n\
			out_Color = vec4(min(color.xyz * difuse + colorambient.xyz * 0.3, 1), 1.0 );\n\
			}\n\
			else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0 );\n\
			}\n\
		}else{\n\
			if(on){\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3 + colorambient.xyz *0.3, 1.0), 1.0);\n\
			}else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0); \n\
			}\n\
		}\n\
		if(toon_1){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			out_Color = vec4(min(color.xyz * difuse, 1), 1.0 );\n\
		}\n\
		if(toon_2){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			\n\
			if(difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			out_Color = vec4(min(color.xyz * difuse + colorlight2.xyz * difuse2, 1), 1.0); \n\
		}\n\
		if(toon_3){\n\
			if (difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			if (difuse3 < 0.2) difuse3 = 0; \n\
			if (difuse3 <= 0.2 && difuse3 < 0.4) difuse3 = 0.2; \n\
			if (difuse3 >= 0.4 && difuse3 < 0.5) difuse3 = 0.4; \n\
			if (difuse3 >= 0.5) difuse3 = 1; \n\
			\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3, 1), 1.0); \n\
			}\n\
		}";
	void setupModel() {
		glGenVertexArrays(1, &modelVao);
		glBindVertexArray(modelVao);
		glGenBuffers(3, modelVbo);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[0]);

		glBufferData(GL_ARRAY_BUFFER, verticesPeus.size() * sizeof(glm::vec3), &verticesPeus[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[1]);

		glBufferData(GL_ARRAY_BUFFER, normalsPeus.size() * sizeof(glm::vec3), &normalsPeus[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);



		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		modelShaders[0] = compileShader(model_vertShader, GL_VERTEX_SHADER, "cubeVert");
		modelShaders[1] = compileShader(model_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		modelProgram = glCreateProgram();
		glAttachShader(modelProgram, modelShaders[0]);
		glAttachShader(modelProgram, modelShaders[1]);
		glBindAttribLocation(modelProgram, 0, "in_Position");
		glBindAttribLocation(modelProgram, 1, "in_Normal");
		linkProgram(modelProgram);
	}
	void cleanupModel() {

		glDeleteBuffers(2, modelVbo);
		glDeleteVertexArrays(1, &modelVao);

		glDeleteProgram(modelProgram);
		glDeleteShader(modelShaders[0]);
		glDeleteShader(modelShaders[1]);
	}
	void updateModel(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawModel(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.004f; //NORIA PEUS

		glm::mat4 s;
		glm::mat4 t;
		s = glm::scale(glm::mat4(), glm::vec3(scal));

		objMat = s;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glDrawArrays(GL_TRIANGLES, 0, 100000);


		
		glUseProgram(0);
		glBindVertexArray(0);

	}

	void drawModel2(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.004f; //NORIA PEUS

		glm::mat4 s;
		glm::mat4 t;
		s = glm::scale(glm::mat4(), glm::vec3(scal));

		objMat = s;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glDrawArrays(GL_TRIANGLES, 0, 100000);


		s = glm::scale(glm::mat4(), glm::vec3(scal));
		t = glm::translate(glm::mat4(), glm::vec3(60.0f, 0.0f, 0.0f));
		objMat = t * s;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glDrawArrays(GL_TRIANGLES, 0, 100000);


		glUseProgram(0);
		glBindVertexArray(0);

	}


}

////////////////////////////////////////////////// MyModelRoda
namespace MyLoadedModelRodaCabina {
	GLuint modelVao;
	GLuint modelVbo[3];
	GLuint modelShaders[2];
	GLuint modelProgram;
	glm::mat4 objMat = glm::mat4(1.f);



	const char* model_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	uniform vec3 lPos;\n\
	uniform vec3 lPos2;\n\
	uniform vec3 lPos3;\n\
	out vec3 lDir;\n\
	out vec3 lDir2;\n\
	out vec3 lDir3;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
		lDir = normalize(lPos - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir2 = normalize(lPos2 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
		lDir3 = normalize(lPos3 - (objMat * vec4(in_Position, 1.0)).xyz );\n\
	}";


	const char* model_fragShader =
		"#version 330\n\
	in vec4 vert_Normal;\n\
	in vec3 lDir;\n\
	in vec3 lDir2;\n\
	in vec3 lDir3;\n\
	out vec4 out_Color;\n\
	uniform mat4 mv_Mat;\n\
	uniform vec4 color;\n\
	uniform vec4 colorlight2;\n\
	uniform vec4 colorlight3;\n\
	uniform vec4 colorambient;\n\
	uniform bool move;\n\
	uniform bool on;\n\
	uniform bool toon_1;\n\
	uniform bool toon_2;\n\
	uniform bool toon_3;\n\
	void main() {\n\
	  float difuse = dot(normalize(vert_Normal), mv_Mat*vec4(lDir.x, lDir.y, lDir.z, 0.0)); \n\
 	  float difuse2 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir2.x, lDir2.y, lDir2.z, 0.0));\n\
	  float difuse3 = dot(normalize(vert_Normal), mv_Mat*vec4(lDir3.x, lDir3.y, lDir3.z, 0.0));\n\
		if(move){\n\
			if(lDir.y > 0){\n\
			out_Color = vec4(min(color.xyz * difuse + colorambient.xyz * 0.3, 1), 1.0 );\n\
			}\n\
			else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0 );\n\
			}\n\
		}else{\n\
			if(on){\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3 + colorambient.xyz *0.3, 1.0), 1.0);\n\
			}else{\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorambient.xyz *0.3, 1.0), 1.0); \n\
			}\n\
		}\n\
		if(toon_1){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			out_Color = vec4(min(color.xyz * difuse, 1), 1.0 );\n\
		}\n\
		if(toon_2){\n\
			if(difuse < 0.2) difuse = 0; \n\
			if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
			if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
			if (difuse >= 0.5) difuse = 1; \n\
			\n\
			if(difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			out_Color = vec4(min(color.xyz * difuse + colorlight2.xyz * difuse2, 1), 1.0); \n\
		}\n\
		if(toon_3){\n\
			if (difuse2 < 0.2) difuse2 = 0; \n\
			if (difuse2 <= 0.2 && difuse2 < 0.4) difuse2 = 0.2; \n\
			if (difuse2 >= 0.4 && difuse2 < 0.5) difuse2 = 0.4; \n\
			if (difuse2 >= 0.5) difuse2 = 1; \n\
			\n\
			if (difuse3 < 0.2) difuse3 = 0; \n\
			if (difuse3 <= 0.2 && difuse3 < 0.4) difuse3 = 0.2; \n\
			if (difuse3 >= 0.4 && difuse3 < 0.5) difuse3 = 0.4; \n\
			if (difuse3 >= 0.5) difuse3 = 1; \n\
			\n\
			out_Color = vec4(min(colorlight2.xyz * difuse2 + colorlight3.xyz * difuse3, 1), 1.0); \n\
			}\n\
		}";
	void setupModel() {
		glGenVertexArrays(1, &modelVao);
		glBindVertexArray(modelVao);
		glGenBuffers(3, modelVbo);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[0]);

		glBufferData(GL_ARRAY_BUFFER, verticesNoria.size() * sizeof(glm::vec3), &verticesNoria[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[1]);

		glBufferData(GL_ARRAY_BUFFER, normalsNoria.size() * sizeof(glm::vec3), &normalsNoria[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);



		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		modelShaders[0] = compileShader(model_vertShader, GL_VERTEX_SHADER, "cubeVert");
		modelShaders[1] = compileShader(model_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		modelProgram = glCreateProgram();
		glAttachShader(modelProgram, modelShaders[0]);
		glAttachShader(modelProgram, modelShaders[1]);
		glBindAttribLocation(modelProgram, 0, "in_Position");
		glBindAttribLocation(modelProgram, 1, "in_Normal");
		linkProgram(modelProgram);
	}
	void cleanupModel() {

		glDeleteBuffers(2, modelVbo);
		glDeleteVertexArrays(1, &modelVao);

		glDeleteProgram(modelProgram);
		glDeleteShader(modelShaders[0]);
		glDeleteShader(modelShaders[1]);
	}
	void updateModel(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawModel(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.004f; //NORIA RODA
		const float GRotacio = glm::radians(65.f);
		
		glm::mat4 s;
		glm::mat4 t;
		glm::mat4 r;
		
		r = glm::rotate(glm::mat4(), GRotacio ,glm::vec3());

		float rot = currentTime;

		r = glm::rotate(glm::mat4(), 0.1f*PI*rot, glm::vec3(0.0f, 0.0, 1.0f));

		s = glm::scale(glm::mat4(), glm::vec3(scal));

		objMat = s*r;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glDrawArrays(GL_TRIANGLES, 0, 100000);



		glUseProgram(0);
		glBindVertexArray(0);

	}

	void drawModel2(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		float scal = 0.004f; //NORIA RODA
		const float GRotacio = glm::radians(65.f);

		glm::mat4 s;
		glm::mat4 t;
		glm::mat4 r;

		r = glm::rotate(glm::mat4(), GRotacio, glm::vec3());

		float rot = currentTime;

		r = glm::rotate(glm::mat4(), -0.1f*PI*rot, glm::vec3(0.0f, 0.0, 1.0f));

		s = glm::scale(glm::mat4(), glm::vec3(scal));

		objMat = s * r;


		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos2"), lightPos2.x, lightPos2.y, lightPos2.z);
		glUniform3f(glGetUniformLocation(modelProgram, "lPos3"), lightPos3.x, lightPos3.y, lightPos3.z);
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight2"), 0.0f, 0.0f, 0.2f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorlight3"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform4f(glGetUniformLocation(modelProgram, "colorambient"), 0.0f, 0.5f, 0.5f, 0.f);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_1"), toon_1);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_2"), toon_2);
		glUniform1i(glGetUniformLocation(modelProgram, "toon_3"), toon_3);
		glUniform1i(glGetUniformLocation(modelProgram, "on"), on);
		glUniform1i(glGetUniformLocation(modelProgram, "move"), light_moves);
		glDrawArrays(GL_TRIANGLES, 0, 100000);


		s = glm::scale(glm::mat4(), glm::vec3(scal));
		t = glm::translate(glm::mat4(), glm::vec3(60.0f, 0.0f, 0.0f));
		r = glm::rotate(glm::mat4(), 0.1f*PI*rot, glm::vec3(0.0f, 0.0, 1.0f));

		objMat = t * s * r;

		glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(modelProgram, "color"), 1.0f, 0.25f + 0.25f*sin((float)currentTime) / 1.5f, 0.0f, 0.0f);
		glDrawArrays(GL_TRIANGLES, 0, 100000);

		glUseProgram(0);
		glBindVertexArray(0);

	}


}

////////////////////////////////////////////////// MyModelCube
namespace MyLoadedModelCube {
	GLuint modelVao;
	GLuint modelVbo[3];
	GLuint modelShaders[2];
	GLuint modelProgram;
	glm::mat4 objMat = glm::mat4(1.f);



	const char* model_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	uniform vec3 lPos;\n\
	out vec3 lDir;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
		lDir = normalize(lPos - gl_Position.xyz );\n\
	}";


	const char* model_fragShader =
		"#version 330\n\
	in vec4 vert_Normal;\n\
	in vec3 lDir;\n\
	out vec4 out_Color;\n\
	uniform mat4 mv_Mat;\n\
	uniform vec4 color;\n\
	void main() {\n\
	  float difuse = dot(normalize(vert_Normal), mv_Mat*vec4(lDir.x, lDir.y, lDir.z, 0.0)); \n\
		out_Color = vec4(color.xyz * difuse , 1.0 );\n\
	}";
	/*
		Toon Shader
		if(difuse < 0.2) difuse = 0; \n\
		if (difuse <= 0.2 && difuse < 0.4) difuse = 0.2; \n\
		if (difuse >= 0.4 && difuse < 0.5) difuse = 0.4; \n\
		if (difuse >= 0.5) difuse = 1; \n\
	*/
	void setupModel() {
		glGenVertexArrays(1, &modelVao);
		glBindVertexArray(modelVao);
		glGenBuffers(3, modelVbo);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[0]);

		glBufferData(GL_ARRAY_BUFFER, verticesCube.size() * sizeof(glm::vec3), &verticesCube[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, modelVbo[1]);

		glBufferData(GL_ARRAY_BUFFER, normalsCube.size() * sizeof(glm::vec3), &normalsCube[0], GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);



		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		modelShaders[0] = compileShader(model_vertShader, GL_VERTEX_SHADER, "cubeVert");
		modelShaders[1] = compileShader(model_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		modelProgram = glCreateProgram();
		glAttachShader(modelProgram, modelShaders[0]);
		glAttachShader(modelProgram, modelShaders[1]);
		glBindAttribLocation(modelProgram, 0, "in_Position");
		glBindAttribLocation(modelProgram, 1, "in_Normal");
		linkProgram(modelProgram);
	}
	void cleanupModel() {

		glDeleteBuffers(2, modelVbo);
		glDeleteVertexArrays(1, &modelVao);

		glDeleteProgram(modelProgram);
		glDeleteShader(modelShaders[0]);
		glDeleteShader(modelShaders[1]);
	}
	void updateModel(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawModel(double currentTime) {

		glBindVertexArray(modelVao);
		glUseProgram(modelProgram);

		glm::mat4 t;

		//traslate * rotate * escalat 
	
		int max_cubes = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;

		for (int i = 0; i <= max_cubes; ++i) {
			t = glm::translate(glm::mat4(), glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * i / max_cubes), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * i / max_cubes), 0));

			objMat = t;

			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
			glUniformMatrix4fv(glGetUniformLocation(modelProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
			glUniform3f(glGetUniformLocation(modelProgram, "lPos"), lightPos.x, lightPos.y, lightPos.z);
			glUniform4f(glGetUniformLocation(modelProgram, "color"), 0.5f, .5f, 1.f, 0.f);
			glDrawArrays(GL_TRIANGLES, 0, 100000);
		}



		glUseProgram(0);
		glBindVertexArray(0);

	}


}

////////////////////////////////////////////////// BOX
namespace Box{
GLuint cubeVao;
GLuint cubeVbo[2];
GLuint cubeShaders[2];
GLuint cubeProgram;

float cubeVerts[] = {
	// -5,0,-5 -- 5, 10, 5
	-5.f,  0.f, -5.f,
	 5.f,  0.f, -5.f,
	 5.f,  0.f,  5.f,
	-5.f,  0.f,  5.f,
	-5.f, 10.f, -5.f,
	 5.f, 10.f, -5.f,
	 5.f, 10.f,  5.f,
	-5.f, 10.f,  5.f,
};
GLubyte cubeIdx[] = {
	1, 0, 2, 3, // Floor - TriangleStrip
	0, 1, 5, 4, // Wall - Lines
	1, 2, 6, 5, // Wall - Lines
	2, 3, 7, 6, // Wall - Lines
	3, 0, 4, 7  // Wall - Lines
};

const char* vertShader_xform =
"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mvpMat;\n\
void main() {\n\
	gl_Position = mvpMat * vec4(in_Position, 1.0);\n\
}";
const char* fragShader_flatColor =
"#version 330\n\
out vec4 out_Color;\n\
uniform vec4 color;\n\
void main() {\n\
	out_Color = color;\n\
}";

void setupCube() {
	glGenVertexArrays(1, &cubeVao);
	glBindVertexArray(cubeVao);
	glGenBuffers(2, cubeVbo);

	glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 24, cubeVerts, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeVbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * 20, cubeIdx, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	cubeShaders[0] = compileShader(vertShader_xform, GL_VERTEX_SHADER, "cubeVert");
	cubeShaders[1] = compileShader(fragShader_flatColor, GL_FRAGMENT_SHADER, "cubeFrag");

	cubeProgram = glCreateProgram();
	glAttachShader(cubeProgram, cubeShaders[0]);
	glAttachShader(cubeProgram, cubeShaders[1]);
	glBindAttribLocation(cubeProgram, 0, "in_Position");
	linkProgram(cubeProgram);
}
void cleanupCube() {
	glDeleteBuffers(2, cubeVbo);
	glDeleteVertexArrays(1, &cubeVao);

	glDeleteProgram(cubeProgram);
	glDeleteShader(cubeShaders[0]);
	glDeleteShader(cubeShaders[1]);
}
void drawCube() {
	glBindVertexArray(cubeVao);
	glUseProgram(cubeProgram);
	glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
	// FLOOR
	glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.6f, 0.6f, 0.6f, 1.f);
	glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, 0);
	// WALLS
	glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 0.f, 0.f, 1.f);
	glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 4));
	glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 8));
	glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 12));
	glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 16));

	glUseProgram(0);
	glBindVertexArray(0);
}
}

////////////////////////////////////////////////// AXIS
namespace Axis {
GLuint AxisVao;
GLuint AxisVbo[3];
GLuint AxisShader[2];
GLuint AxisProgram;

float AxisVerts[] = {
	0.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	0.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 0.0, 0.0,
	0.0, 0.0, 1.0
};
float AxisColors[] = {
	1.0, 0.0, 0.0, 1.0,
	1.0, 0.0, 0.0, 1.0,
	0.0, 1.0, 0.0, 1.0,
	0.0, 1.0, 0.0, 1.0,
	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 1.0, 1.0
};
GLubyte AxisIdx[] = {
	0, 1,
	2, 3,
	4, 5
};
const char* Axis_vertShader =
"#version 330\n\
in vec3 in_Position;\n\
in vec4 in_Color;\n\
out vec4 vert_color;\n\
uniform mat4 mvpMat;\n\
void main() {\n\
	vert_color = in_Color;\n\
	gl_Position = mvpMat * vec4(in_Position, 1.0);\n\
}";
const char* Axis_fragShader =
"#version 330\n\
in vec4 vert_color;\n\
out vec4 out_Color;\n\
void main() {\n\
	out_Color = vert_color;\n\
}";

void setupAxis() {
	glGenVertexArrays(1, &AxisVao);
	glBindVertexArray(AxisVao);
	glGenBuffers(3, AxisVbo);

	glBindBuffer(GL_ARRAY_BUFFER, AxisVbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 24, AxisVerts, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, AxisVbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 24, AxisColors, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 4, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, AxisVbo[2]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * 6, AxisIdx, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	AxisShader[0] = compileShader(Axis_vertShader, GL_VERTEX_SHADER, "AxisVert");
	AxisShader[1] = compileShader(Axis_fragShader, GL_FRAGMENT_SHADER, "AxisFrag");

	AxisProgram = glCreateProgram();
	glAttachShader(AxisProgram, AxisShader[0]);
	glAttachShader(AxisProgram, AxisShader[1]);
	glBindAttribLocation(AxisProgram, 0, "in_Position");
	glBindAttribLocation(AxisProgram, 1, "in_Color");
	linkProgram(AxisProgram);
}
void cleanupAxis() {
	glDeleteBuffers(3, AxisVbo);
	glDeleteVertexArrays(1, &AxisVao);

	glDeleteProgram(AxisProgram);
	glDeleteShader(AxisShader[0]);
	glDeleteShader(AxisShader[1]);
}
void drawAxis() {
	glBindVertexArray(AxisVao);
	glUseProgram(AxisProgram);
	glUniformMatrix4fv(glGetUniformLocation(AxisProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
	glDrawElements(GL_LINES, 6, GL_UNSIGNED_BYTE, 0);

	glUseProgram(0);
	glBindVertexArray(0);
}
}

////////////////////////////////////////////////// SPHERE
namespace Sphere {
GLuint sphereVao;
GLuint sphereVbo;
GLuint sphereShaders[3];
GLuint sphereProgram;
float radius = 10;

const char* sphere_vertShader =
"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mv_Mat;\n\
void main() {\n\
	gl_Position = mv_Mat * vec4(in_Position, 1.0);\n\
}";
const char* sphere_geomShader =
"#version 330\n\
layout(points) in;\n\
layout(triangle_strip, max_vertices = 4) out;\n\
out vec4 eyePos;\n\
out vec4 centerEyePos;\n\
uniform mat4 projMat;\n\
uniform float radius;\n\
vec4 nu_verts[4];\n\
void main() {\n\
	vec3 n = normalize(-gl_in[0].gl_Position.xyz);\n\
	vec3 up = vec3(0.0, 1.0, 0.0);\n\
	vec3 u = normalize(cross(up, n));\n\
	vec3 v = normalize(cross(n, u));\n\
	nu_verts[0] = vec4(-radius*u - radius*v, 0.0); \n\
	nu_verts[1] = vec4( radius*u - radius*v, 0.0); \n\
	nu_verts[2] = vec4(-radius*u + radius*v, 0.0); \n\
	nu_verts[3] = vec4( radius*u + radius*v, 0.0); \n\
	centerEyePos = gl_in[0].gl_Position;\n\
	for (int i = 0; i < 4; ++i) {\n\
		eyePos = (gl_in[0].gl_Position + nu_verts[i]);\n\
		gl_Position = projMat * eyePos;\n\
		EmitVertex();\n\
	}\n\
	EndPrimitive();\n\
}";
const char* sphere_fragShader_flatColor =
"#version 330\n\
in vec4 eyePos;\n\
in vec4 centerEyePos;\n\
out vec4 out_Color;\n\
uniform mat4 projMat;\n\
uniform mat4 mv_Mat;\n\
uniform vec4 color;\n\
uniform float radius;\n\
void main() {\n\
	vec4 diff = eyePos - centerEyePos;\n\
	float distSq2C = dot(diff, diff);\n\
	if (distSq2C > (radius*radius)) discard;\n\
	float h = sqrt(radius*radius - distSq2C);\n\
	vec4 nuEyePos = vec4(eyePos.xy, eyePos.z + h, 1.0);\n\
	vec4 nuPos = projMat * nuEyePos;\n\
	gl_FragDepth = ((nuPos.z / nuPos.w) + 1) * 0.5;\n\
	vec3 normal = normalize(nuEyePos - centerEyePos).xyz;\n\
	out_Color = vec4(color.xyz * dot(normal, (mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)).xyz) + color.xyz * 0.3, 1.0 );\n\
}";

bool shadersCreated = false;
void createSphereShaderAndProgram() {
	if(shadersCreated) return;

	sphereShaders[0] = compileShader(sphere_vertShader, GL_VERTEX_SHADER, "sphereVert");
	sphereShaders[1] = compileShader(sphere_geomShader, GL_GEOMETRY_SHADER, "sphereGeom");
	sphereShaders[2] = compileShader(sphere_fragShader_flatColor, GL_FRAGMENT_SHADER, "sphereFrag");

	sphereProgram = glCreateProgram();
	glAttachShader(sphereProgram, sphereShaders[0]);
	glAttachShader(sphereProgram, sphereShaders[1]);
	glAttachShader(sphereProgram, sphereShaders[2]);
	glBindAttribLocation(sphereProgram, 0, "in_Position");
	linkProgram(sphereProgram);

	shadersCreated = true;
}
void cleanupSphereShaderAndProgram() {
	if(!shadersCreated) return;
	glDeleteProgram(sphereProgram);
	glDeleteShader(sphereShaders[0]);
	glDeleteShader(sphereShaders[1]);
	glDeleteShader(sphereShaders[2]);
	shadersCreated = false;
}

void setupSphere(glm::vec3 pos, float radius) {
	Sphere::radius = radius;
	glGenVertexArrays(1, &sphereVao);
	glBindVertexArray(sphereVao);
	glGenBuffers(1, &sphereVbo);

	glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3, &pos, GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	createSphereShaderAndProgram();
}
void cleanupSphere() {
	glDeleteBuffers(1, &sphereVbo);
	glDeleteVertexArrays(1, &sphereVao);

	cleanupSphereShaderAndProgram();
}
void updateSphere(glm::vec3 pos, float radius) {
	glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
	float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	buff[0] = pos.x;
	buff[1] = pos.y;
	buff[2] = pos.z;
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	Sphere::radius = radius;
}
void drawSphere() {
	glBindVertexArray(sphereVao);
	glUseProgram(sphereProgram);
	glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
	glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
	glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
	glUniform4f(glGetUniformLocation(sphereProgram, "color"), 0.6f, 0.1f, 0.1f, 1.f);
	glUniform1f(glGetUniformLocation(sphereProgram, "radius"), Sphere::radius);
	glDrawArrays(GL_POINTS, 0, 1);

	glUseProgram(0);
	glBindVertexArray(0);
}
}

////////////////////////////////////////////////// SPHERE 2
namespace Sphere2 {
	GLuint sphereVao;
	GLuint sphereVbo;
	GLuint sphereShaders[3];
	GLuint sphereProgram;
	float radius = 10;

	const char* sphere_vertShader =
		"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mv_Mat;\n\
void main() {\n\
	gl_Position = mv_Mat * vec4(in_Position, 1.0);\n\
}";
	const char* sphere_geomShader =
		"#version 330\n\
layout(points) in;\n\
layout(triangle_strip, max_vertices = 4) out;\n\
out vec4 eyePos;\n\
out vec4 centerEyePos;\n\
uniform mat4 projMat;\n\
uniform float radius;\n\
vec4 nu_verts[4];\n\
void main() {\n\
	vec3 n = normalize(-gl_in[0].gl_Position.xyz);\n\
	vec3 up = vec3(0.0, 1.0, 0.0);\n\
	vec3 u = normalize(cross(up, n));\n\
	vec3 v = normalize(cross(n, u));\n\
	nu_verts[0] = vec4(-radius*u - radius*v, 0.0); \n\
	nu_verts[1] = vec4( radius*u - radius*v, 0.0); \n\
	nu_verts[2] = vec4(-radius*u + radius*v, 0.0); \n\
	nu_verts[3] = vec4( radius*u + radius*v, 0.0); \n\
	centerEyePos = gl_in[0].gl_Position;\n\
	for (int i = 0; i < 4; ++i) {\n\
		eyePos = (gl_in[0].gl_Position + nu_verts[i]);\n\
		gl_Position = projMat * eyePos;\n\
		EmitVertex();\n\
	}\n\
	EndPrimitive();\n\
}";
	const char* sphere_fragShader_flatColor =
		"#version 330\n\
in vec4 eyePos;\n\
in vec4 centerEyePos;\n\
out vec4 out_Color;\n\
uniform mat4 projMat;\n\
uniform mat4 mv_Mat;\n\
uniform vec4 color;\n\
uniform float radius;\n\
void main() {\n\
	vec4 diff = eyePos - centerEyePos;\n\
	float distSq2C = dot(diff, diff);\n\
	if (distSq2C > (radius*radius)) discard;\n\
	float h = sqrt(radius*radius - distSq2C);\n\
	vec4 nuEyePos = vec4(eyePos.xy, eyePos.z + h, 1.0);\n\
	vec4 nuPos = projMat * nuEyePos;\n\
	gl_FragDepth = ((nuPos.z / nuPos.w) + 1) * 0.5;\n\
	vec3 normal = normalize(nuEyePos - centerEyePos).xyz;\n\
	out_Color = vec4(color.xyz * dot(normal, (mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)).xyz) + color.xyz * 0.3, 1.0 );\n\
}";

	bool shadersCreated = false;
	void createSphereShaderAndProgram() {
		if (shadersCreated) return;

		sphereShaders[0] = compileShader(sphere_vertShader, GL_VERTEX_SHADER, "sphereVert");
		sphereShaders[1] = compileShader(sphere_geomShader, GL_GEOMETRY_SHADER, "sphereGeom");
		sphereShaders[2] = compileShader(sphere_fragShader_flatColor, GL_FRAGMENT_SHADER, "sphereFrag");

		sphereProgram = glCreateProgram();
		glAttachShader(sphereProgram, sphereShaders[0]);
		glAttachShader(sphereProgram, sphereShaders[1]);
		glAttachShader(sphereProgram, sphereShaders[2]);
		glBindAttribLocation(sphereProgram, 0, "in_Position");
		linkProgram(sphereProgram);

		shadersCreated = true;
	}
	void cleanupSphereShaderAndProgram() {
		if (!shadersCreated) return;
		glDeleteProgram(sphereProgram);
		glDeleteShader(sphereShaders[0]);
		glDeleteShader(sphereShaders[1]);
		glDeleteShader(sphereShaders[2]);
		shadersCreated = false;
	}

	void setupSphere(glm::vec3 pos, float radius) {
		Sphere::radius = radius;
		glGenVertexArrays(1, &sphereVao);
		glBindVertexArray(sphereVao);
		glGenBuffers(1, &sphereVbo);

		glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3, &pos, GL_DYNAMIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		createSphereShaderAndProgram();
	}
	void cleanupSphere() {
		glDeleteBuffers(1, &sphereVbo);
		glDeleteVertexArrays(1, &sphereVao);

		cleanupSphereShaderAndProgram();
	}
	void updateSphere(glm::vec3 pos, float radius) {
		glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
		float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		buff[0] = pos.x;
		buff[1] = pos.y;
		buff[2] = pos.z;
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		Sphere::radius = radius;
	}
	void drawSphere() {
		glBindVertexArray(sphereVao);
		glUseProgram(sphereProgram);
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
		glUniform4f(glGetUniformLocation(sphereProgram, "color"), 0.1f, 0.1f, 0.6f, 1.f);
		glUniform1f(glGetUniformLocation(sphereProgram, "radius"), Sphere::radius);
		glDrawArrays(GL_POINTS, 0, 1);

		glUseProgram(0);
		glBindVertexArray(0);
	}
}


////////////////////////////////////////////////// SPHERE 3
namespace Sphere3 {
	GLuint sphereVao;
	GLuint sphereVbo;
	GLuint sphereShaders[3];
	GLuint sphereProgram;
	glm::mat4 objMat = glm::mat4(1.f);
	float radius = 10;

	const char* sphere_vertShader =
		"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mv_Mat;\n\
uniform mat4 mvpMat;\n\
uniform mat4 objMat;\n\
void main() {\n\
	gl_Position = mv_Mat * vec4(in_Position, 1.0);\n\
}";
	const char* sphere_geomShader =
		"#version 330\n\
layout(points) in;\n\
layout(triangle_strip, max_vertices = 4) out;\n\
out vec4 eyePos;\n\
out vec4 centerEyePos;\n\
uniform mat4 projMat;\n\
uniform float radius;\n\
vec4 nu_verts[4];\n\
void main() {\n\
	vec3 n = normalize(-gl_in[0].gl_Position.xyz);\n\
	vec3 up = vec3(0.0, 1.0, 0.0);\n\
	vec3 u = normalize(cross(up, n));\n\
	vec3 v = normalize(cross(n, u));\n\
	nu_verts[0] = vec4(-radius*u - radius*v, 0.0); \n\
	nu_verts[1] = vec4( radius*u - radius*v, 0.0); \n\
	nu_verts[2] = vec4(-radius*u + radius*v, 0.0); \n\
	nu_verts[3] = vec4( radius*u + radius*v, 0.0); \n\
	centerEyePos = gl_in[0].gl_Position;\n\
	for (int i = 0; i < 4; ++i) {\n\
		eyePos = (gl_in[0].gl_Position + nu_verts[i]);\n\
		gl_Position = projMat * eyePos;\n\
		EmitVertex();\n\
	}\n\
	EndPrimitive();\n\
}";
	const char* sphere_fragShader_flatColor =
		"#version 330\n\
in vec4 eyePos;\n\
in vec4 centerEyePos;\n\
out vec4 out_Color;\n\
uniform mat4 projMat;\n\
uniform mat4 mv_Mat;\n\
uniform vec4 color;\n\
uniform float radius;\n\
void main() {\n\
	vec4 diff = eyePos - centerEyePos;\n\
	float distSq2C = dot(diff, diff);\n\
	if (distSq2C > (radius*radius)) discard;\n\
	float h = sqrt(radius*radius - distSq2C);\n\
	vec4 nuEyePos = vec4(eyePos.xy, eyePos.z + h, 1.0);\n\
	vec4 nuPos = projMat * nuEyePos;\n\
	gl_FragDepth = ((nuPos.z / nuPos.w) + 1) * 0.5;\n\
	vec3 normal = normalize(nuEyePos - centerEyePos).xyz;\n\
	out_Color = vec4(color.xyz * dot(normal, (mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)).xyz) + color.xyz * 0.3, 1.0 );\n\
}";

	bool shadersCreated = false;
	void createSphereShaderAndProgram() {
		if (shadersCreated) return;

		sphereShaders[0] = compileShader(sphere_vertShader, GL_VERTEX_SHADER, "sphereVert");
		sphereShaders[1] = compileShader(sphere_geomShader, GL_GEOMETRY_SHADER, "sphereGeom");
		sphereShaders[2] = compileShader(sphere_fragShader_flatColor, GL_FRAGMENT_SHADER, "sphereFrag");

		sphereProgram = glCreateProgram();
		glAttachShader(sphereProgram, sphereShaders[0]);
		glAttachShader(sphereProgram, sphereShaders[1]);
		glAttachShader(sphereProgram, sphereShaders[2]);
		glBindAttribLocation(sphereProgram, 0, "in_Position");
		linkProgram(sphereProgram);

		shadersCreated = true;
	}
	void cleanupSphereShaderAndProgram() {
		if (!shadersCreated) return;
		glDeleteProgram(sphereProgram);
		glDeleteShader(sphereShaders[0]);
		glDeleteShader(sphereShaders[1]);
		glDeleteShader(sphereShaders[2]);
		shadersCreated = false;
	}

	void setupSphere(glm::vec3 pos, float radius) {
		Sphere::radius = radius;
		glGenVertexArrays(1, &sphereVao);
		glBindVertexArray(sphereVao);
		glGenBuffers(1, &sphereVbo);

		glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3, &pos, GL_DYNAMIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		createSphereShaderAndProgram();
	}
	void cleanupSphere() {
		glDeleteBuffers(1, &sphereVbo);
		glDeleteVertexArrays(1, &sphereVao);

		cleanupSphereShaderAndProgram();
	}
	void updateSphere(glm::vec3 pos, float radius) {
		glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
		float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
		buff[0] = pos.x;
		buff[1] = pos.y;
		buff[2] = pos.z;
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		Sphere::radius = radius;
	}
	void updateModel(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawSphere(double currentTime) {


		glm::mat4 t;

		int max_cabins = 20;
		float frequencia = 0.05f;
		float radius = 22.0f;
		t = glm::translate(glm::mat4(), glm::vec3(radius* cos(2 * PI *frequencia*currentTime + 2 * PI * 1 / max_cabins), radius*sin(2 * PI*frequencia*currentTime + 2 * PI * 1 / max_cabins), 0));
		objMat = t;
		glBindVertexArray(sphereVao);
		glUseProgram(sphereProgram);
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(sphereProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
		glUniform4f(glGetUniformLocation(sphereProgram, "color"), 0.0f, 0.6f, 0.6f, 1.f);
		glUniform1f(glGetUniformLocation(sphereProgram, "radius"), Sphere::radius);
		glDrawArrays(GL_POINTS, 0, 1);

		glUseProgram(0);
		glBindVertexArray(0);
	}
}

////////////////////////////////////////////////// CAPSULE
namespace Capsule {
GLuint capsuleVao;
GLuint capsuleVbo[2];
GLuint capsuleShader[3];
GLuint capsuleProgram;
float radius;

const char* capsule_vertShader =
"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mv_Mat;\n\
void main() {\n\
	gl_Position = mv_Mat * vec4(in_Position, 1.0);\n\
}";
const char* capsule_geomShader =
"#version 330\n\
layout(lines) in; \n\
layout(triangle_strip, max_vertices = 14) out;\n\
out vec3 eyePos;\n\
out vec3 capPoints[2];\n\
uniform mat4 projMat;\n\
uniform float radius;\n\
vec3 boxVerts[8];\n\
int boxIdx[14];\n\
void main(){\n\
	vec3 A = gl_in[0].gl_Position.xyz;\n\
	vec3 B = gl_in[1].gl_Position.xyz;\n\
	if(gl_in[1].gl_Position.x < gl_in[0].gl_Position.x) {\n\
		A = gl_in[1].gl_Position.xyz;\n\
		B = gl_in[0].gl_Position.xyz;\n\
	}\n\
	vec3 u = vec3(0.0, 1.0, 0.0);\n\
	if (abs(dot(u, normalize(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz))) - 1.0 < 1e-6) {\n\
		if(gl_in[1].gl_Position.y > gl_in[0].gl_Position.y) {\n\
			A = gl_in[1].gl_Position.xyz;\n\
			B = gl_in[0].gl_Position.xyz;\n\
		}\n\
		u = vec3(1.0, 0.0, 0.0);\n\
	}\n\
	vec3 Am = normalize(A - B); \n\
	vec3 Bp = -Am;\n\
	vec3 v = normalize(cross(Am, u)) * radius;\n\
	u = normalize(cross(v, Am)) * radius;\n\
	Am *= radius;\n\
	Bp *= radius;\n\
	boxVerts[0] = A + Am - u - v;\n\
	boxVerts[1] = A + Am + u - v;\n\
	boxVerts[2] = A + Am + u + v;\n\
	boxVerts[3] = A + Am - u + v;\n\
	boxVerts[4] = B + Bp - u - v;\n\
	boxVerts[5] = B + Bp + u - v;\n\
	boxVerts[6] = B + Bp + u + v;\n\
	boxVerts[7] = B + Bp - u + v;\n\
	boxIdx = int[](0, 3, 4, 7, 6, 3, 2, 1, 6, 5, 4, 1, 0, 3);\n\
	capPoints[0] = A;\n\
	capPoints[1] = B;\n\
	for (int i = 0; i<14; ++i) {\n\
		eyePos = boxVerts[boxIdx[i]];\n\
		gl_Position = projMat * vec4(boxVerts[boxIdx[i]], 1.0);\n\
		EmitVertex();\n\
	}\n\
	EndPrimitive();\n\
}";
const char* capsule_fragShader_flatColor =
"#version 330\n\
in vec3 eyePos;\n\
in vec3 capPoints[2];\n\
out vec4 out_Color;\n\
uniform mat4 projMat;\n\
uniform mat4 mv_Mat;\n\
uniform vec4 color;\n\
uniform float radius;\n\
const int lin_steps = 30;\n\
const int bin_steps = 5;\n\
vec3 closestPointInSegment(vec3 p, vec3 a, vec3 b) {\n\
	vec3 pa = p - a, ba = b - a;\n\
	float h = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);\n\
	return a + ba*h;\n\
}\n\
void main() {\n\
	vec3 viewDir = normalize(eyePos);\n\
	float step = radius / 5.0;\n\
	vec3 nuPB = eyePos;\n\
	int i = 0;\n\
	for(i = 0; i < lin_steps; ++i) {\n\
		nuPB = eyePos + viewDir*step*i;\n\
		vec3 C = closestPointInSegment(nuPB, capPoints[0], capPoints[1]);\n\
		float dist = length(C - nuPB) - radius;\n\
		if(dist < 0.0) break;\n\
	}\n\
	if(i==lin_steps) discard;\n\
	vec3 nuPA = nuPB - viewDir*step;\n\
	vec3 C;\n\
	for(i = 0; i < bin_steps; ++i) {\n\
		vec3 nuPC = nuPA + (nuPB - nuPA)*0.5; \n\
		C = closestPointInSegment(nuPC, capPoints[0], capPoints[1]); \n\
		float dist = length(C - nuPC) - radius; \n\
		if(dist > 0.0) nuPA = nuPC; \n\
		else nuPB = nuPC; \n\
	}\n\
	vec4 nuPos = projMat * vec4(nuPA, 1.0);\n\
	gl_FragDepth = ((nuPos.z / nuPos.w) + 1) * 0.5;\n\
	vec3 normal = normalize(nuPA - C);\n\
	out_Color = vec4(color.xyz * dot(normal, (mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)).xyz) + color.xyz * 0.3, 1.0 );\n\
}";

void setupCapsule(glm::vec3 posA, glm::vec3 posB, float radius) {
	Capsule::radius = radius;
	glGenVertexArrays(1, &capsuleVao);
	glBindVertexArray(capsuleVao);
	glGenBuffers(2, capsuleVbo);

	float capsuleVerts[] = {
		posA.x, posA.y, posA.z, 
		posB.x, posB.y, posB.z
	};
	GLubyte capsuleIdx[] = {
		0, 1
	};

	glBindBuffer(GL_ARRAY_BUFFER, capsuleVbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6, capsuleVerts, GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, capsuleVbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * 2, capsuleIdx, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	capsuleShader[0] = compileShader(capsule_vertShader, GL_VERTEX_SHADER, "capsuleVert");
	capsuleShader[1] = compileShader(capsule_geomShader, GL_GEOMETRY_SHADER, "capsuleGeom");
	capsuleShader[2] = compileShader(capsule_fragShader_flatColor, GL_FRAGMENT_SHADER, "capsuleFrag");

	capsuleProgram = glCreateProgram();
	glAttachShader(capsuleProgram, capsuleShader[0]);
	glAttachShader(capsuleProgram, capsuleShader[1]);
	glAttachShader(capsuleProgram, capsuleShader[2]);
	glBindAttribLocation(capsuleProgram, 0, "in_Position");
	linkProgram(capsuleProgram);
}
void cleanupCapsule() {
	glDeleteBuffers(2, capsuleVbo);
	glDeleteVertexArrays(1, &capsuleVao);

	glDeleteProgram(capsuleProgram);
	glDeleteShader(capsuleShader[0]);
	glDeleteShader(capsuleShader[1]);
	glDeleteShader(capsuleShader[2]);
}
void updateCapsule(glm::vec3 posA, glm::vec3 posB, float radius) {
	float vertPos[] = {posA.x, posA.y, posA.z, posB.z, posB.y, posB.z};
	glBindBuffer(GL_ARRAY_BUFFER, capsuleVbo[0]);
	float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	buff[0] = posA.x; buff[1] = posA.y; buff[2] = posA.z;
	buff[3] = posB.x; buff[4] = posB.y; buff[5] = posB.z;
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	Capsule::radius = radius;
}
void drawCapsule() {
	glBindVertexArray(capsuleVao);
	glUseProgram(capsuleProgram);
	glUniformMatrix4fv(glGetUniformLocation(capsuleProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
	glUniformMatrix4fv(glGetUniformLocation(capsuleProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
	glUniformMatrix4fv(glGetUniformLocation(capsuleProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
	glUniform4fv(glGetUniformLocation(capsuleProgram, "camPoint"), 1, &RV::_cameraPoint[0]);
	glUniform4f(glGetUniformLocation(capsuleProgram, "color"), 0.1f, 0.6f, 0.1f, 1.f);
	glUniform1f(glGetUniformLocation(capsuleProgram, "radius"), Capsule::radius);
	glDrawElements(GL_LINES, 2, GL_UNSIGNED_BYTE, 0);

	glUseProgram(0);
	glBindVertexArray(0);
}
}

////////////////////////////////////////////////// PARTICLES
// Same rendering as Sphere (reusing shaders)
namespace LilSpheres {
GLuint particlesVao;
GLuint particlesVbo;
float radius;
int numparticles;
extern const int maxParticles = SHRT_MAX;

void setupParticles(int numTotalParticles, float radius) {
	assert(numTotalParticles > 0);
	assert(numTotalParticles <= SHRT_MAX);
	numparticles = numTotalParticles;
	LilSpheres::radius = radius;
	
	glGenVertexArrays(1, &particlesVao);
	glBindVertexArray(particlesVao);
	glGenBuffers(1, &particlesVbo);

	glBindBuffer(GL_ARRAY_BUFFER, particlesVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * numparticles, 0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	Sphere::createSphereShaderAndProgram();
}
void cleanupParticles() {
	glDeleteVertexArrays(1, &particlesVao);
	glDeleteBuffers(1, &particlesVbo);

	Sphere::cleanupSphereShaderAndProgram();
}
void updateParticles(int startIdx, int count, float* array_data) {
	glBindBuffer(GL_ARRAY_BUFFER, particlesVbo);
	float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	buff = &buff[3*startIdx];
	for(int i = 0; i < 3*count; ++i) {
		buff[i] = array_data[i];
	}
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void drawParticles(int startIdx, int count) {
	glBindVertexArray(particlesVao);
	glUseProgram(Sphere::sphereProgram);
	glUniformMatrix4fv(glGetUniformLocation(Sphere::sphereProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
	glUniformMatrix4fv(glGetUniformLocation(Sphere::sphereProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RV::_modelView));
	glUniformMatrix4fv(glGetUniformLocation(Sphere::sphereProgram, "projMat"), 1, GL_FALSE, glm::value_ptr(RV::_projection));
	glUniform4f(glGetUniformLocation(Sphere::sphereProgram, "color"), 0.1f, 0.1f, 0.6f, 1.f);
	glUniform1f(glGetUniformLocation(Sphere::sphereProgram, "radius"), LilSpheres::radius);
	glDrawArrays(GL_POINTS, startIdx, count);

	glUseProgram(0);
	glBindVertexArray(0);
}
}

////////////////////////////////////////////////// CLOTH
namespace ClothMesh {
GLuint clothVao;
GLuint clothVbo[2];
GLuint clothShaders[2];
GLuint clothProgram;
extern const int numCols = 14;
extern const int numRows = 18;
extern const int numVerts = numRows * numCols;
int numVirtualVerts;

const char* cloth_vertShader =
"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mvpMat;\n\
void main() {\n\
	gl_Position = mvpMat * vec4(in_Position, 1.0);\n\
}";
const char* cloth_fragShader =
"#version 330\n\
uniform vec4 color;\n\
out vec4 out_Color;\n\
void main() {\n\
	out_Color = color;\n\
}";

void setupClothMesh() {
	glGenVertexArrays(1, &clothVao);
	glBindVertexArray(clothVao);
	glGenBuffers(2, clothVbo);

	glBindBuffer(GL_ARRAY_BUFFER, clothVbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 3 * numVerts, 0, GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glPrimitiveRestartIndex(UCHAR_MAX);
	constexpr int facesVertsIdx = 5 * (numCols - 1) * (numRows - 1);
	GLubyte facesIdx[facesVertsIdx] = { 0 };
	for (int i = 0; i < (numRows - 1); ++i) {
		for (int j = 0; j < (numCols - 1); ++j) {
			facesIdx[5 * (i*(numCols-1) + j) + 0] = i*numCols + j;
			facesIdx[5 * (i*(numCols-1) + j) + 1] = (i + 1)*numCols + j;
			facesIdx[5 * (i*(numCols-1) + j) + 2] = (i + 1)*numCols + (j + 1);
			facesIdx[5 * (i*(numCols-1) + j) + 3] = i*numCols + (j + 1);
			facesIdx[5 * (i*(numCols-1) + j) + 4] = UCHAR_MAX;
		}
	}
	numVirtualVerts = facesVertsIdx;

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, clothVbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte)*numVirtualVerts, facesIdx, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	clothShaders[0] = compileShader(cloth_vertShader, GL_VERTEX_SHADER, "clothVert");
	clothShaders[1] = compileShader(cloth_fragShader, GL_FRAGMENT_SHADER, "clothFrag");

	clothProgram = glCreateProgram();
	glAttachShader(clothProgram, clothShaders[0]);
	glAttachShader(clothProgram, clothShaders[1]);
	glBindAttribLocation(clothProgram, 0, "in_Position");
	linkProgram(clothProgram);
}
void cleanupClothMesh() {
	glDeleteBuffers(2, clothVbo);
	glDeleteVertexArrays(1, &clothVao);

	glDeleteProgram(clothProgram);
	glDeleteShader(clothShaders[0]);
	glDeleteShader(clothShaders[1]);
}
void updateClothMesh(float *array_data) {
	glBindBuffer(GL_ARRAY_BUFFER, clothVbo[0]);
	float* buff = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	for (int i = 0; i < 3 * numVerts; ++i) {
		buff[i] = array_data[i];
	}
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void drawClothMesh() {
	glEnable(GL_PRIMITIVE_RESTART);
	glBindVertexArray(clothVao);
	glUseProgram(clothProgram);
	glUniformMatrix4fv(glGetUniformLocation(clothProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
	glUniform4f(glGetUniformLocation(clothProgram, "color"), 0.1f, 1.f, 1.f, 0.f);
	glDrawElements(GL_LINE_LOOP, numVirtualVerts, GL_UNSIGNED_BYTE, 0);

	glUseProgram(0);
	glBindVertexArray(0);
	glDisable(GL_PRIMITIVE_RESTART);
}
}


